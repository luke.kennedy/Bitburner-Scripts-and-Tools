package main

import (
	"fmt"
	"strconv"
)

const testString = "5aaabb450723abb"
const testOutput = "aaabbaaababababaabb"

const realString = "92fl2p3k1v03KOB669YpWWWqWW009yXXsDWvAW882B7792z2521F582bI72"

func main() {
	out := lzDecompress(realString)
	fmt.Printf("%s\n", out)
}

func lzDecompress(input string) string {
	outData := ""
	mode1 := true
	for i := 0; i < len(input); i++ {
		lenStr := input[i]
		chunkLength, err := strconv.Atoi(string([]byte{lenStr}))
		if err != nil {
			panic(err)
		}
		if chunkLength == 0 {
			fmt.Printf("Skipping\n")
			if mode1 {
				mode1 = false
			} else {
				mode1 = true
			}
			continue
		}
		if mode1 {
			fmt.Printf("Front-filling\n")
			mode1 = false
			subStr := input[i+1 : i+1+chunkLength]
			i += chunkLength
			outData += subStr
		} else {
			fmt.Printf("Back-filling\n")
			mode1 = true
			next := input[i+1]
			lookback, err := strconv.Atoi(string([]byte{next}))
			if err != nil {
				panic(err)
			}
			outStart := len(outData) - lookback
			for j := 0; j < chunkLength; j++ {
				outData += string([]byte{outData[outStart+j]})
			}
			i += 1
		}
	}
	return outData
}
