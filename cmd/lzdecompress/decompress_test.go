package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestLZDecompress(t *testing.T) {
	assert.Equal(t, testOutput, lzDecompress(testString))
}
