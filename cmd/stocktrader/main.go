package main

import "log"

var stockPrices = []int{99, 36, 119, 1, 7, 129, 57, 191, 84, 102, 175, 148, 85, 195, 135, 158, 3, 165, 136, 151, 175, 86, 99, 29, 91, 189, 111, 197, 66, 197, 86, 168, 105, 11, 90, 114, 83, 115, 6}
var stockPrices2 = []int{69, 91, 191, 149, 38, 59, 71, 34, 159, 124, 5, 141, 35, 170, 115, 2, 195, 82, 142}

func main() {
	stonks := stockPrices2
	maxPurchases := 2
	maxProfit, buySells := getMaxProfit(stonks, maxPurchases, 0)
	log.Printf("Profit: %d\n", maxProfit)
	log.Printf("Buys and sells: %v\n", buySells)
}

func getMaxProfit(prices []int, maxPurchases, currentPurchases int) (int, []int) {
	buySells := []int{}
	maxProfit := 0
	for i, buyPrice := range prices {
		for j := i + 1; j < len(prices); j++ {
			sellPrice := prices[j]
			profit := sellPrice - buyPrice
			var extraProfit int
			var extraBuySells []int
			if currentPurchases+1 < maxPurchases && len(prices) > j+2 {
				extraProfit, extraBuySells = getMaxProfit(prices[j+1:], maxPurchases, currentPurchases+1)
				profit += extraProfit
			}
			if profit > maxProfit {
				maxProfit = profit
				buySells = []int{prices[i], prices[j]}
				buySells = append(buySells, extraBuySells...)
			}
		}
	}
	return maxProfit, buySells
}
